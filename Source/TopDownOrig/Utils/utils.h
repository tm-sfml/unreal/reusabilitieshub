#pragma once
#include "EngineUtils.h"
#include <Runtime/Engine/Classes/GameFramework/Actor.h>
#include <Utils/macro_shortcuts.h>
#include <algorithm>

class AActor;

// TODO: change to nested namespaces when visual studio intellisence will support it
// manipulating world objects, dealing with their "world" properties - e.g position
// world wide actions
namespace WorldUtils
{
template <class TActor>
TActor* FindActorInWorld(UWorld* world, FString const& name)
{
    if (!world)
    {
        UE_LOG(LogTemp, Error, TEXT("world is null"));
        return nullptr;
    }

    FString actorName;
    auto isOurActor = [&name, &actorName](TActor* actor) {
        actor->GetName(actorName);
        return actorName == name;
    };

    TActorIterator<TActor> end(EActorIteratorType::End);
    auto it = std::find_if(TActorIterator<TActor>(world), end, isOurActor);
    TActor* res = (it == end) ? nullptr : *it;
    return res;
}

template <class TActor>
TActor* FindFirstActorInWorld(UWorld* world)
{
    TActorIterator<TActor> const begin(world);
    return begin == TActorIterator<TActor>(EActorIteratorType::End) ? nullptr : *begin;
}

template <class TActor>
TActor* FindActorByTag(UWorld* world, FName const& tag)
{
    auto isOurActor = [&tag](TActor* actor) {
        return actor->Tags.Contains(tag);
    };

    TActorIterator<TActor> const end(EActorIteratorType::End);
    auto it = std::find_if(TActorIterator<TActor>(world), end, isOurActor);
    return (it == end) ? nullptr : *it;
}

template <class TActor, class TFilter>
TActor* FindClosetActor(UWorld* world, TFilter&& filter, AActor* queryActor)
{
    if (!world)
    {
        ERROR_LOG("world is null");
        return nullptr;
    }

    float currMinDistance = TNumericLimits<float>::Max();
    TActor* currClosestCharacter = nullptr;
    for (TActorIterator<TActor> it(world); it; ++it)
    {
        auto* actor = *it;
        //auto isEnemyTag = [this](FName const& tag)
        //{
        //    return EnemyActorTags.Contains(tag);
        //};
        //bool const isEnemy = character->Tags.ContainsByPredicate(isEnemyTag);
        if (!filter(*actor))
        {
            continue;
        }
        //if (!isEnemy)
        //{
        //    continue;
        //}

        float const dist = WorldUtils::Distance(queryActor, actor);
        if (dist < currMinDistance)
        {
            currClosestCharacter = actor;
            currMinDistance = dist;
        }
    }

    return currClosestCharacter;
}

template <class TChildActor>
TChildActor* AddChild(AActor* owner)
{
    UChildActorComponent* child = NewObject<UChildActorComponent>(owner);
    child->bEditableWhenInherited = true;
    child->RegisterComponent();
    child->SetChildActorClass(TChildActor::StaticClass());
    child->CreateChildActor();
    return static_cast<TChildActor*>(child->GetChildActor());
}

template <class TComponent>
TComponent* AddComponent(AActor* owner)
{
    auto* spawnedComponent = NewObject<TComponent>(owner);
    if (spawnedComponent)
    {
        spawnedComponent->RegisterComponent();
        spawnedComponent->AttachToComponent(owner->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
    }
    return spawnedComponent;
}

float Distance(AActor const& from, AActor const& to);
float Distance(AActor const* from, AActor const* to);

float GetDistanceHandleRadius(AActor const* from, AActor const* to);
float GetRadius(AActor const* actor);

/** find intersection of ray in world space with ground plane */
FVector IntersectRayWithPlane(const FVector& RayOrigin, const FVector& RayDirection, const FPlane& Plane);
} // namespace WorldUtils

namespace C3Utils
{
template <class TCallable>
FInputActionBinding ActionBindingFromLambda(FName actionName, EInputEvent keyEvent, TCallable&& callable)
{
    FInputActionBinding binding(actionName, keyEvent);
    binding.ActionDelegate.GetDelegateForManualSet().BindLambda(callable);
    return binding;
}

template <class TCallable>
void BindLambdaToAction(FName actionName, EInputEvent keyEvent, TCallable&& callable, UInputComponent* input)
{
    input->AddActionBinding(ActionBindingFromLambda(actionName, keyEvent, callable));
}
} // namespace C3Utils