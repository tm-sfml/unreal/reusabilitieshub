#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AReferenceHolder.generated.h"

UENUM()
enum class WorldActorReference
{
    CursorDecal UMETA(DisplayName = "CursorDecal"),
    TileMap UMETA(DisplayName = "TileMap")
};

// actor that is used to get a reference to other actors, placed on the scene in editor,
// when it is not possible to do so in a normal way (e.g player controller, pawn)
// all such references are grouped into single actor
UCLASS()
class TOPDOWNORIG_API AAReferenceHolder : public AActor
{
    GENERATED_BODY()
public:
    static AAReferenceHolder* GetInstance();

    template <class TActor>
    TActor* GetReference(WorldActorReference key)
    {
        AActor* reference = References.FindRef(key);
        return Cast<TActor>(reference);
    }

protected:
    void PreInitializeComponents() override;

private:
    UPROPERTY(EditAnywhere, Category = References, meta = (AllowPrivateAccess = "true"))
    TMap<WorldActorReference, AActor*> References;

    inline static AAReferenceHolder* Instance = nullptr;
};
