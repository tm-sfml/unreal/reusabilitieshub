#include "utils.h"

namespace WorldUtils
{
float Distance(AActor const& from, AActor const& to)
{
    float dist = FVector::Distance(from.GetActorLocation(), to.GetActorLocation());
    return dist;
}

float Distance(AActor const* from, AActor const* to)
{
    if (!ENSURE(from) || !ENSURE(to))
    {
        return 0.f;
    }
    return Distance(*from, *to);
}

float GetDistanceHandleRadius(AActor const* from, AActor const* to)
{
    return Distance(from, to) - GetRadius(from) - GetRadius(to);
}

float GetRadius(AActor const* actor)
{
    if (!ENSURE(actor))
    {
        return 0.f;
    }

    FVector _;
    FVector ourExtent;
    actor->GetActorBounds(true, _, ourExtent);
    return (ourExtent.X + ourExtent.Y) / 2.f;
}

FVector IntersectRayWithPlane(const FVector& RayOrigin, const FVector& RayDirection, const FPlane& Plane)
{
    const FVector PlaneNormal = FVector(Plane.X, Plane.Y, Plane.Z);
    const FVector PlaneOrigin = PlaneNormal * Plane.W;

    const float Distance = FVector::DotProduct((PlaneOrigin - RayOrigin), PlaneNormal) / FVector::DotProduct(RayDirection, PlaneNormal);
    return RayOrigin + RayDirection * Distance;
}
} // namespace WorldUtils