#pragma once

#define DEBUG_LOG(message) UE_LOG(LogTemp, Display, message);
#define DEBUG_LOGF(format, ...) UE_LOG(LogTemp, Display, TEXT(format), __VA_ARGS__);

//#define DEBUG_LOG_VAR(variable) UE_LOG(LogTemp, Display, TEXT("%s: %s"));


// logs the name of current function
#define DEBUG_LOG_FUNC() UE_LOG(LogTemp, Display, TEXT(__FUNCTION__));
#define ERROR_LOG(message) UE_LOG(LogTemp, Error, TEXT(__FUNCTION__ " : " message));
#define ERROR_LOGF(format, ...) UE_LOG(LogTemp, Error, TEXT(__FUNCTION__ " : " format), __VA_ARGS__);
#define ERROR_LOG_IF(condition, message) \
    if (condition)                       \
    {                                    \
        ERROR_LOG(message);              \
    }


// breaks in debugger prints msg + callstack if condition is false,
#define ENSURE_MSG(condition, message) ensureAlwaysMsgf(condition, TEXT(message))
#define ENSURE(condition) ensureAlways(condition)
