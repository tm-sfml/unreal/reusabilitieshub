#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tilemap.generated.h"

UCLASS()
class TOPDOWNORIG_API ATilemap : public AActor
{
    GENERATED_BODY()

public:
    ATilemap();

private:
    UPROPERTY(VisibleAnywhere, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    class UJsonTilemapReaderComponent* JsonTilemapReader;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    class UTileGraphComponent* TileGraph;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    class UWorldTilemapComponent* WorldTilemap;

protected:
    void OnConstruction(const FTransform& Transform);
};
