#pragma once

#include "CoreMinimal.h"
//#include "TArray2D.generated.h"

/**
 * Wrapper for using array as 2-dimentional array
 */
template <class TElem>
class TOPDOWNORIG_API TArray2D // : public UObject
{
public:
    using SizeType = typename TArray<TElem>::SizeType;

    TArray2D() = default;

    TArray2D(FIntPoint dimentions)
        : FlatArray(dimentions.X * dimentions.Y)
        , Size2D(dimentions.Y)
    {}

    TArray<TElem>& GetFlatArray() { return FlatArray; }
    int GetSize1D() { return FlatArray.Num() / Size2D; }
    int GetSize2D() { return Size2D; }

    bool isValidIndex(FIntPoint index) const
    {
        return FlatArray.IsValidIndex(FlattenIndex(index));
    }

    void Init(TElem const& fillElement, FIntPoint dimentions)
    {
        Size2D = dimentions.Y;
        FlatArray.Init(fillElement, dimentions.X * dimentions.Y);
    }

    void InitDefaulted(FIntPoint dimentions)
    {
        Size2D = dimentions.Y;
        FlatArray.InsertDefaulted(0, dimentions.X * dimentions.Y);
    }

    void SetNumUninitialized(FIntPoint dimentions)
    {
        FlatArray.SetNumZeroed();
    }

    TElem const& operator[](FIntPoint index) const
    {
        return FlatArray[FlattenIndex(index)];
    }

    TElem& operator[](FIntPoint index)
    {
        // why the hell does this not compile?
        //return const_cast<TElem&>(std::as_const(*this)[index]);
        return FlatArray[FlattenIndex(index)];
    }

private:
    int Size2D = 0;
    TArray<TElem> FlatArray;

    int FlattenIndex(FIntPoint index) const
    {
        return index.X * Size2D + index.Y;
    }
};
