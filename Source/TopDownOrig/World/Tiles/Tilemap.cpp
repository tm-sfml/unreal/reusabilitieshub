#include "Tilemap.h"
#include <World/Tiles/JsonTilemapReaderComponent.h>
#include <World/Tiles/TileGraph.h>
#include <World/Tiles/TileHighlightComponent.h>
#include <World/Tiles/WorldTilemapComponent.h>
#include <Utils/macro_shortcuts.h>

ATilemap::ATilemap()
    : Super()
{
    JsonTilemapReader = CreateDefaultSubobject<UJsonTilemapReaderComponent>("JsonTilemapReader");
    TileGraph = CreateDefaultSubobject<UTileGraphComponent>("TileGraph");
    WorldTilemap = CreateDefaultSubobject<UWorldTilemapComponent>("WorldTilemapComponent");

    bRunConstructionScriptOnDrag = false;
}

void ATilemap::OnConstruction(const FTransform& Transform)
{
    Super::OnConstruction(Transform);

    // calling manually in OnConstruction so that bRunConstructionScriptOnDrag can be disabled + dependencies order
    JsonTilemapReader->Parse();
    TileGraph->OnOwnerCreated();
    WorldTilemap->OnOwnerCreated();
}
