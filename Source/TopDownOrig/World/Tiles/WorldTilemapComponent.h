#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "WorldTilemapComponent.generated.h"

class UStaticMesh;
class UInstancedStaticMeshComponent;

// generates tilemap in the world, handles all the physical (in-world) aspects of tiles
// depends on UJsonTilemapReaderComponent
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNORIG_API UWorldTilemapComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    FVector GetTileDimentions() const;
    TMap<FIntPoint, AActor*> GetStartingCharacters() const { return StartingCharacters; }

    // x is along width, y - along height
    FIntPoint GetTileIndex(FVector tileHitPoint);
    FTransform GetTileCenterTransform(FIntPoint coords, int layer) const;
    FVector GetTileCenter(FIntPoint coords, int layer) const;

    // generates tilemap from data parsed by UJsonTilemapReaderComponent and places starting characters above tiles
    // tiles are placed along 2 axes - right (X) for width, forward (Y) for height,
    // if multiple layers are present, each constitutes additional vertical floor
    // all tile dimentions are equal, meshes should be of equal size
    // Owner actor's location is in the back-left-bottom corner of the (0,0) tile
    // called manually in owner's construction script
    void OnOwnerCreated();

protected:
    #if WITH_EDITOR
    void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
    #endif

private:
    // map { Tiled tile index : Tile mesh}
    // first mesh defines the bounds of a tile
    UPROPERTY(EditAnywhere, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    TArray<UStaticMesh*> TileMeshes;

    UPROPERTY(EditAnywhere, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    float Interval = 10.f;

    UPROPERTY(EditAnywhere, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    bool ShowCoordinatesLabels = true;

    // references to editor actors which will be placed
    UPROPERTY(EditInstanceOnly, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    TMap<FIntPoint, AActor*> StartingCharacters;
    inline static auto const StartingCharactersPropertyName = "StartingCharacters";

    // one for each user-provided mesh
    TArray<UInstancedStaticMeshComponent*> InstancedTileMeshes;

    void SpawnTile(FIntPoint coords, int layer, int tileIndex);
    void InitInstanceMeshes();

    FVector GetTileCenterLocal(FIntPoint coords, int layer) const;
    void PlaceStartingActors();
};
