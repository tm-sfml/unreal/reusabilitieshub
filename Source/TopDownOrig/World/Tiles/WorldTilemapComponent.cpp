#include "WorldTilemapComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include <Components\TextRenderComponent.h>
#include <Engine\TextRenderActor.h>
#include <World/Tiles/JsonTilemapReaderComponent.h>
#include <TopDownOrig\Utils\macro_shortcuts.h>
#include <TopDownOrig\Utils\utils.h>
#include <chrono>
#include <numeric>

FIntPoint UWorldTilemapComponent::GetTileIndex(FVector tileHitPoint)
{
    if (TileMeshes.Num() == 0)
    {
        ERROR_LOG("TileMeshes empty");
        return {};
    }

    FVector const localPoint = GetOwner()->GetActorTransform().InverseTransformPosition(tileHitPoint);

    FVector const meshDims = TileMeshes[0]->GetBounds().GetBox().GetSize();
    return {FMath::FloorToInt(localPoint.Y / (meshDims.Y + Interval)),
            FMath::FloorToInt(localPoint.X / (meshDims.X + Interval))};
}

FTransform UWorldTilemapComponent::GetTileCenterTransform(FIntPoint coords, int layer) const
{
    FTransform tileTransform = GetOwner()->GetTransform();
    tileTransform.AddToTranslation(GetTileCenterLocal(coords, layer));
    return tileTransform;
}

FVector UWorldTilemapComponent::GetTileCenter(FIntPoint coords, int layer) const
{
    return GetOwner()->GetActorLocation() + GetTileCenterLocal(coords, layer);
}

FVector UWorldTilemapComponent::GetTileDimentions() const
{
    return TileMeshes[0]->GetBounds().GetBox().GetSize();
}

void UWorldTilemapComponent::OnOwnerCreated()
{
    auto* tilemapReader = GetOwner()->FindComponentByClass<UJsonTilemapReaderComponent>();
    if (!tilemapReader)
    {
        ERROR_LOG("tilemapReader not found");
        return;
    }

    if (TileMeshes.Num() == 0 || !TileMeshes[0])
    {
        ERROR_LOG("TileMeshes empty");
        return;
    }
    InitInstanceMeshes();

    if (!tilemapReader->GetTilemap())
    {
        ERROR_LOG("no tilemap");
        return;
    }

    auto const& layers = tilemapReader->GetTilemap().GetValue();
    FIntPoint const dimentions = tilemapReader->GetDimentions();

    for (int i = 0; i < layers.Num(); i++)
    {
        TSharedPtr<FJsonObject> const& layer = layers[i]->AsObject();
        TArray<TSharedPtr<FJsonValue>> const& tilesArray = layer->GetArrayField(TEXT("data"));

        for (int j = 0; j < tilesArray.Num(); j++)
        {
            int const tileIndex = static_cast<int>(tilesArray[j]->AsNumber());
            // in tiled, 0 means empty
            if (tileIndex != 0)
            {
                SpawnTile({j % dimentions.X, dimentions.Y - (j / dimentions.X) - 1}, i, tileIndex - 1);
            }
        }
    }

    for (auto it = StartingCharacters.CreateIterator(); it; ++it)
    {
        // TODO: check if there is no obstacle
        auto& [tile, character] = *it;

        if (!character)
        {
            ERROR_LOG("character is null");
            return;
        }

        FVector boundsExtent, _;
        character->GetActorBounds(false, _, boundsExtent);

        auto characterCenter = GetTileCenter(tile, 0) + FVector::UpVector * boundsExtent.Z;
        character->SetActorLocation(characterCenter);
    }
}

void UWorldTilemapComponent::PostEditChangeProperty(FPropertyChangedEvent& event)
{
    Super::PostEditChangeProperty(event);

    if (event.GetPropertyName() != StartingCharactersPropertyName)
    {
        return;
    }

    for (auto it = StartingCharacters.CreateIterator(); it; ++it)
    {
        // TODO: check if there is no obstacle
        auto& [tile, character] = *it;

        if (!character)
        {
            ERROR_LOG("character is null");
            return;
        }

        FVector boundsExtent, _;
        character->GetActorBounds(false, _, boundsExtent);

        auto characterCenter = GetTileCenter(tile, 0) + FVector::UpVector * boundsExtent.Z;
        character->SetActorLocation(characterCenter);
    }
}

void UWorldTilemapComponent::SpawnTile(FIntPoint coords, int layer, int tileIndex)
{
    if (!TileMeshes.IsValidIndex(tileIndex) || !TileMeshes[tileIndex])
    {
        ERROR_LOGF(TEXT("tile mesh missing for index %d"), tileIndex);
        return;
    }

    InstancedTileMeshes[tileIndex]->AddInstanceWorldSpace(GetTileCenterTransform(coords, layer));

    if (ShowCoordinatesLabels)
    {
        auto* indexLabel = NewObject<UTextRenderComponent>(GetOwner()->GetRootComponent());
        indexLabel->RegisterComponent();
        indexLabel->AttachToComponent(GetOwner()->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);

        FTransform labelTransform(FQuat(FRotator(90.f, 180.f, 0.f)), GetTileCenterLocal(coords, layer) + FVector::UpVector * GetTileDimentions().Y / 2 + 1.f);
        indexLabel->SetRelativeTransform(labelTransform);

        indexLabel->SetText(FText::FromString(FString::Printf(L"x: %d<br>y: %d", coords.X, coords.Y)));
        indexLabel->SetHorizontalAlignment(EHTA_Center);
        indexLabel->SetVerticalAlignment(EVRTA_TextCenter);
    }
}

void UWorldTilemapComponent::InitInstanceMeshes()
{
    InstancedTileMeshes.SetNum(TileMeshes.Num());
    for (size_t i = 0; i < TileMeshes.Num(); i++)
    {
        InstancedTileMeshes[i] = WorldUtils::AddComponent<UInstancedStaticMeshComponent>(GetOwner());
        InstancedTileMeshes[i]->SetStaticMesh(TileMeshes[i]);
    }
}

FVector UWorldTilemapComponent::GetTileCenterLocal(FIntPoint coords, int layer) const
{
    FVector const tileDimentions = GetTileDimentions();

    // TODO: use AddInstance (local space) - can't get it to work...
    FVector const tileLocalOffset = FVector::ForwardVector * coords.Y * (tileDimentions.X + Interval) +
                                    FVector::RightVector * coords.X * (tileDimentions.Y + Interval) +
                                    FVector::UpVector * tileDimentions.Z * layer +
                                    tileDimentions / 2.f;

    return tileLocalOffset;
}

void UWorldTilemapComponent::PlaceStartingActors()
{
}
