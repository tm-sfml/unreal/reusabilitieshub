#include "TileGraph.h"
#include "Engine/Engine.h"
#include <World/Tiles/JsonTilemapReaderComponent.h>
#include <World/Tiles/WorldTilemapComponent.h>
#include <Utils/macro_shortcuts.h>

void UTileGraphComponent::BeginPlay()
{
    Super::BeginPlay();
    FillObstacles();

    auto const* worldTilemap = GetOwner()->FindComponentByClass<UWorldTilemapComponent>();
    if (!worldTilemap)
    {
        ERROR_LOG("worldTilemap not found");
        return;
    }

    for (auto it = worldTilemap->GetStartingCharacters().CreateIterator(); it; ++it)
    {
        TileNodes[it->Key].OccupyingCharacter = it->Value;
    }
}

void UTileGraphComponent::OnOwnerCreated()
{
    auto* tilemapReader = GetOwner()->FindComponentByClass<UJsonTilemapReaderComponent>();
    if (!tilemapReader)
    {
        ERROR_LOG("tilemapReader not found");
        return;
    }

    if (!tilemapReader->GetTilemap())
    {
        ERROR_LOG("no tilemap");
        return;
    }

    FIntPoint const dimentions = tilemapReader->GetDimentions();
    Width = dimentions.X;
    Height = dimentions.Y;
}

void UTileGraphComponent::FillObstacles()
{
    auto* tilemapReader = GetOwner()->FindComponentByClass<UJsonTilemapReaderComponent>();
    if (!tilemapReader)
    {
        ERROR_LOG("tilemapReader not found");
        return;
    }

    // parsing again, because prev time was on editor(construction) time, and there is no way to preserve json until the runtime
    ERROR_LOG_IF(tilemapReader->GetTilemap(), "already parsed");
    tilemapReader->Parse();
    auto const& layers = tilemapReader->GetTilemap().GetValue();

    TileNodes.InitDefaulted({Width, Height});
    for (int i = 0; i < layers.Num(); i++)
    {
        TSharedPtr<FJsonObject> const& layer = layers[i]->AsObject();
        TArray<TSharedPtr<FJsonValue>> const& tilesArray = layer->GetArrayField(TEXT("data"));

        for (int j = 0; j < tilesArray.Num(); j++)
        {
            int const tileIndex = static_cast<int>(tilesArray[j]->AsNumber());
            bool const isObstacle = tileIndex != 0 && i != 0;
            TileNodes[{i, j}].IsObstacle = isObstacle;
        }
    }
}
