#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include <World/Tiles/Array2D.h>
#include "TileHighlightComponent.generated.h"


class UStaticMesh;
class UInstancedStaticMeshComponent;
class UWorldTilemapComponent;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNORIG_API UTileHighlightComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    bool IsTileHighlighted(FIntPoint tileIndex) const;
    void HighlightTile(FIntPoint tileIndex);
    // TODO: can't delete individual instances because their array is re-shuffled on delete
    void UnhighlightTile(FIntPoint tileIndex);
    void UnhighlightAllTiles();

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

private:
    UPROPERTY(EditAnywhere, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    UStaticMesh* TileHighlightMesh = nullptr;

    inline static float const HighlightMeshHeight = 5.f;

    UInstancedStaticMeshComponent* HighlightInstancedMesh = nullptr;

    // map {tile index(2D): index of highlight mesh instance, or INT_MAX if tile is not highlighted}
    TArray2D<int> HighlightMeshInstances;

};
