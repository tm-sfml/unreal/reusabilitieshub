#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include <World/Tiles/Array2D.h>
#include "TileGraph.generated.h"

struct TileData
{
    // false for walls, etc, true if taken by character
    bool IsObstacle = false;
    AActor* OccupyingCharacter = nullptr;
};

// depends on UJsonTilemapReaderComponent, WorldTilemap components
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNORIG_API UTileGraphComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    int GetWidth() const { return Width; }
    int GetHeight() const { return Height; }

    TArray<FIntPoint> FindPath(FIntPoint from, FIntPoint to) const;

    void OnOwnerCreated();

protected:
    void BeginPlay() override;

private:
    // uproperties since they are loaded at construction and persist through play
    UPROPERTY()
    int Width = 0;
    UPROPERTY()
    int Height = 0;

    // map {tile index(2D): data}
    TArray2D<TileData> TileNodes;
    void FillObstacles();
};
