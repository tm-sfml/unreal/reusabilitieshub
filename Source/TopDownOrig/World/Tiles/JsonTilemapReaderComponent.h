#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Json.h"
#include "JsonTilemapReaderComponent.generated.h"

// parses tiles json and holds it during whole construction time, clears it on begin play
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNORIG_API UJsonTilemapReaderComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    using JsonTilemap = TOptional<TArray<TSharedPtr<FJsonValue>>>;

    JsonTilemap const& GetTilemap() const { return Tilemap; }
    FIntPoint GetDimentions() const;

    // parses json tilemap
    void Parse();

private:
    // Tiled map, exported as json
    UPROPERTY(EditAnywhere, Category = Tiles, meta = (AllowPrivateAccess = "true"))
    FString TilemapFilename;

    // TODO: parse into a structure that can persist from editor to play
    JsonTilemap Tilemap;

    JsonTilemap ParseTilemap() const;
};
