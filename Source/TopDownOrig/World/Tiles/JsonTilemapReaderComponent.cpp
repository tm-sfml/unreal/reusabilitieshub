#include "JsonTilemapReaderComponent.h"
#include <TopDownOrig\Utils\macro_shortcuts.h>

FIntPoint UJsonTilemapReaderComponent::GetDimentions() const
{
    if (!Tilemap || Tilemap->Num() == 0)
    {
        return {};
    }

    TSharedPtr<FJsonObject> const firstLayer = Tilemap.GetValue()[0]->AsObject();

    return {(int) firstLayer->GetNumberField(TEXT("width")),
            (int) firstLayer->GetNumberField(TEXT("height"))};
}

void UJsonTilemapReaderComponent::Parse()
{
    Tilemap = ParseTilemap();
}

UJsonTilemapReaderComponent::JsonTilemap UJsonTilemapReaderComponent::ParseTilemap() const
{
    FString FullPath = FPaths::ProjectContentDir() + TilemapFilename;
    FString JsonStr;
    bool const success = FFileHelper::LoadFileToString(JsonStr, *FullPath);
    if (!success)
    {
        ERROR_LOGF("map file not found: %s", *TilemapFilename);
        return {};
    }

    TSharedRef<TJsonReader<TCHAR>> JsonReader = FJsonStringReader::Create(JsonStr);
    TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
    bool const parsed = FJsonSerializer::Deserialize(JsonReader, JsonObject);

    if (!parsed)
    {
        ERROR_LOG("json parse error");
        return {};
    }

    return JsonObject->GetArrayField(TEXT("layers"));
}
