#include "TileHighlightComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include <World/Tiles/TileGraph.h>
#include <World/Tiles/WorldTilemapComponent.h>
#include <Utils/macro_shortcuts.h>
#include <Utils/utils.h>

namespace
{
int const UnghighlightedTileId = INT_MAX;
}

// Called when the game starts
void UTileHighlightComponent::BeginPlay()
{
    Super::BeginPlay();

    if (!TileHighlightMesh)
    {
        ERROR_LOG("TileHighlightMesh not set");
        return;
    }
    HighlightInstancedMesh = WorldUtils::AddComponent<UInstancedStaticMeshComponent>(GetOwner());
    HighlightInstancedMesh->SetStaticMesh(TileHighlightMesh);

    auto const* tileGraph = GetOwner()->FindComponentByClass<UTileGraphComponent>();
    if (!tileGraph)
    {
        ERROR_LOG("tileGraph not found");
        return;
    }

    HighlightMeshInstances.Init(INT_MAX, {tileGraph->GetWidth(), tileGraph->GetHeight()});
}


bool UTileHighlightComponent::IsTileHighlighted(FIntPoint tileIndex) const
{
    return HighlightMeshInstances[tileIndex] != INT_MAX;
}


void UTileHighlightComponent::HighlightTile(FIntPoint tileIndex)
{
    if (IsTileHighlighted(tileIndex))
    {
        return;
    }

    auto const* worldTiles = GetOwner()->FindComponentByClass<UWorldTilemapComponent>();
    if (!worldTiles)
    {
        ERROR_LOG("worldTiles not found");
        return;
    }

    FTransform tileCenter = worldTiles->GetTileCenterTransform(tileIndex, 0);
    tileCenter.AddToTranslation(FVector::UpVector * (worldTiles->GetTileDimentions().Z / 2.f + HighlightMeshHeight));

    if (HighlightInstancedMesh)
    {
        int const tileInstance = HighlightInstancedMesh->AddInstanceWorldSpace(tileCenter);
        HighlightMeshInstances[tileIndex] = tileInstance;
    }
}

void UTileHighlightComponent::UnhighlightAllTiles()
{
    HighlightInstancedMesh->ClearInstances();
    HighlightMeshInstances.Init(INT_MAX, {HighlightMeshInstances.GetSize1D(), HighlightMeshInstances.GetSize2D()});
}
