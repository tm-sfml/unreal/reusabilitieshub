#pragma once
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MeleeAttackComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNORIG_API UMeleeAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
    UPROPERTY(EditAnywhere, Category = ProjectileAttack, meta=(AllowPrivateAccess = "true"))
    float Range = 0.f;

	// Sets default values for this component's properties
	UMeleeAttackComponent();
    bool IsInRange(AActor* target) const;

    // fires continuosly at target until it goes out of range or StopFiring is called
    void Attack(AActor* target, TFunction<void()> onOutOfRange, TFunction<void()> onSingleAttack = nullptr);
    void StopAttack();

private:
    UPROPERTY(EditAnywhere, Category = Attack, meta=(AllowPrivateAccess = "true"))
    float AllowedRangeError = 0.f;

    UPROPERTY(EditAnywhere, Category = Attack, meta=(AllowPrivateAccess = "true"))
    float BetweenAttackDelay = 0.f;

    UPROPERTY(EditAnywhere, Category = Attack, meta=(AllowPrivateAccess = "true"))
    float Damage = 0.f;

    AActor* Target = nullptr;
    FTimerHandle ReloadTimer;
    FTimerHandle OutOfRangeCheckTimer;
    TFunction<void()> OnSingleAttack;
    TFunction<void()> OnAttackEnd;

    void OnReloaded();
    void StopIfOutOfRange();
    static float GetCapsuleRadius(AActor* actor);
};
