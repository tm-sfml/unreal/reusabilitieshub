#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "CreepBehavior.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNORIG_API UCreepBehavior : public UActorComponent
{
    GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, Category = ProjectileAttack, meta = (AllowPrivateAccess = true))
    TArray<FName> EnemyActorTags;

    UPROPERTY(EditAnywhere, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    AActor* FightAreaMarker = nullptr;

    UPROPERTY(EditAnywhere, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    AActor* MoveMarker = nullptr;
};
