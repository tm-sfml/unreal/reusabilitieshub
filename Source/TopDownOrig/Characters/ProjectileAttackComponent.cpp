// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileAttackComponent.h"
#include <Engine/World.h>
#include "GameFramework/Actor.h"
#include <GameFramework/ProjectileMovementComponent.h>
#include <Utils/macro_shortcuts.h>
#include <Utils/utils.h>

namespace
{
    float const OutOfRangeCheckInterval = 0.3f;
}

// Sets default values for this component's properties
UProjectileAttackComponent::UProjectileAttackComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UProjectileAttackComponent::BeginPlay()
{
    Super::BeginPlay();
    ENSURE(ProjectileSpawnPoint);
    ENSURE(ProjectileClass);
}

bool UProjectileAttackComponent::IsInRange(AActor* target) const
{
    if (!ENSURE(target))
    {
        return false;
    }
    return WorldUtils::Distance(*GetOwner(), *target) < Range;
}

void UProjectileAttackComponent::FireAt(AActor* target, TFunction<void()> onFireEnd)
{
    if (!ENSURE(target))
    {
        return;
    }

    OnFireEnd = onFireEnd;
    Target = target;
        
    auto& timerManager = GetWorld()->GetTimerManager();
    timerManager.SetTimer(ReloadTimer, this, &ThisClass::OnReloaded, BetweenShotDelay, true); 
    timerManager.SetTimer(OutOfRangeCheckTimer, this, &ThisClass::StopIfOutOfRange, OutOfRangeCheckInterval, true, BetweenShotDelay);
}

void UProjectileAttackComponent::StopFiring()
{
    DEBUG_LOG_FUNC()
    if (ENSURE(OnFireEnd))
    {
        OnFireEnd();
    }
    Target = nullptr;

    auto& timerManager = GetWorld()->GetTimerManager();
    timerManager.ClearTimer(ReloadTimer);   
    timerManager.ClearTimer(OutOfRangeCheckTimer);
}

void UProjectileAttackComponent::OnReloaded()
{
    if (!ENSURE(Target))
    {
        return;
    }

    FActorSpawnParameters spawnParams;
	spawnParams.Owner = GetOwner();
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

    FVector const spawnPos = ProjectileSpawnPoint->GetComponentToWorld().GetLocation();
	// spawn the projectile at the muzzle
     auto* projectileActor = GetWorld()->SpawnActor<AActor>(ProjectileClass, spawnPos, FRotator(), spawnParams);
     auto* projectile = projectileActor->FindComponentByClass<UProjectileMovementComponent>();
     
     projectile->bIsHomingProjectile = true;
     projectile->HomingTargetComponent = Target->GetRootComponent();
}

void UProjectileAttackComponent::StopIfOutOfRange()
{
    if (!ENSURE(Target))
    {
        return;
    }

    if (!IsInRange(Target))
    {
        StopFiring();
    }
}
