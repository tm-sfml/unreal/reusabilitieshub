#include "HealthComponent.h"
#include <GameFramework/Actor.h>
#include <Utils/macro_shortcuts.h>

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UHealthComponent::BeginPlay()
{
    Super::BeginPlay();
    ENSURE(MaxHealth > 0);
    Health = MaxHealth;

    if (DestroyOwnerOnDeath)
    {
        OnDeath.AddLambda([owner = GetOwner()] { owner->Destroy(); });
    }
}

float UHealthComponent::GetHealthPercentage() const
{
    return static_cast<float>(Health) / static_cast<float>(MaxHealth);
}

bool UHealthComponent::TakeDamage(int amount)
{
    if (!IsAlive())
    {
        ERROR_LOG("health < 0");
        return true;
    }

    Health -= amount;
    bool const isDead = !IsAlive();
    if (isDead)
    {
        OnDeath.Broadcast();
    }
    return isDead;
}
