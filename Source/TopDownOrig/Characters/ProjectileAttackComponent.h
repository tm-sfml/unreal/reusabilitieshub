// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ProjectileAttackComponent.generated.h"

class UProjectileMovementComponent;
class USceneComponent;

// range attack firing projectiles
// strategy style - no line of sight check, ai firing as long as target is in radius
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNORIG_API UProjectileAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
    // added to our parent, configured in editor
    UPROPERTY(EditAnywhere, Category = ProjectileAttack, meta=(AllowPrivateAccess = "true"))
    USceneComponent* ProjectileSpawnPoint = nullptr;

	// Sets default values for this component's properties
	UProjectileAttackComponent();
    void BeginPlay() override;
    bool IsInRange(AActor* target) const;

    // fires continuosly at target until it goes out of range or StopFiring is called
    void FireAt(AActor* target, TFunction<void()> onOutOfRange);
    // fires until maxShots have been done, or target became out of range
    void FireAt(AActor* target, TFunction<void()> onEnd, int maxShots);
    void StopFiring();

private:
    UPROPERTY(EditAnywhere, Category = ProjectileAttack, meta=(AllowPrivateAccess = "true"))
    float Range = 0.f;

    UPROPERTY(EditAnywhere, Category = ProjectileAttack, meta=(AllowPrivateAccess = "true"))
    float BetweenShotDelay = 0.f;

    UPROPERTY(EditAnywhere, Category = ProjectileAttack, meta=(AllowPrivateAccess = "true"))
    TSubclassOf<AActor> ProjectileClass = nullptr;

    AActor* Target = nullptr;
    FTimerHandle ReloadTimer;
    FTimerHandle OutOfRangeCheckTimer;
    TFunction<void()> OnFireEnd;

    void OnReloaded();
    void StopIfOutOfRange();
};
