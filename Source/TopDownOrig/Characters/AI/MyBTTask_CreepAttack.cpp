#include "MyBTTask_CreepAttack.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BlackboardComponent.h"
#include <Characters/MeleeAttackComponent.h>
#include <AIController.h>
#include <Utils/macro_shortcuts.h>
#include <Characters/HealthComponent.h>

EBTNodeResult::Type UMyBTTask_CreepAttack::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8* NodeMemory)
{
    auto* aiActor = ownerComp.GetAIOwner()->GetPawn();
    ENSURE(aiActor);
    auto* melee = aiActor->FindComponentByClass<UMeleeAttackComponent>();
    if (!ENSURE(melee))
    {
        return EBTNodeResult::Failed;
    }

    UObject* bBValue = ownerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(BlackboardKey.SelectedKeyName);
    auto* target = static_cast<AActor*>(bBValue);
    if (!target)
    {
        ERROR_LOG("target is null");
        return EBTNodeResult::Failed;
    }

    if (!melee->IsInRange(target))
    {
        ERROR_LOG("melee - not in range");
        return EBTNodeResult::Failed;
    }

    auto onEachAttack = [this, ownerCompPtr = &ownerComp, melee] {
        // HACK for object only
        auto* conditionObject = ownerCompPtr->GetBlackboardComponent()->GetValueAsObject(StopAttackConditionKey.SelectedKeyName);
        if (conditionObject)
        {
            FinishLatentTask(*ownerCompPtr, EBTNodeResult::Aborted);
            melee->StopAttack();
            DEBUG_LOG(L"Stopped attack on condition");
        }
    };

    auto onFireEnd = [this, ownerCompPtr = &ownerComp, target] {
        FinishLatentTask(*ownerCompPtr, EBTNodeResult::Succeeded);
    };

    if (UseStopAttackCondition)
    {
        melee->Attack(target, onFireEnd, onEachAttack);
    }
    else
    {
        melee->Attack(target, onFireEnd);
    }

    auto* health = target->FindComponentByClass<UHealthComponent>();
    if (!health)
    {
        ERROR_LOG("health missing");
        return EBTNodeResult::Failed;
    }

    FBlackboardKeySelector s;
    s.SelectedKeyName = L"not there";
    auto& blackboard = *ownerComp.GetBlackboardComponent();
    s.ResolveSelectedKey(*blackboard.GetBlackboardAsset());
    bool const keyPresent = blackboard.IsValidKey(s.GetSelectedKeyID());

    StopAttackConditionKey.ResolveSelectedKey(*blackboard.GetBlackboardAsset());
    bool const keyPresent2 = blackboard.IsValidKey(StopAttackConditionKey.GetSelectedKeyID());

    auto onTargetDeath = [this, &ownerComp, health] {
        ownerComp.GetBlackboardComponent()->SetValueAsObject(BlackboardKey.SelectedKeyName, nullptr);
        health->OnDeath.Remove(OnTargetDeathDelegateHandle);
        FinishLatentAbort(ownerComp);
    };

    OnTargetDeathDelegateHandle = health->OnDeath.AddLambda(onTargetDeath);

    return EBTNodeResult::InProgress;
}
