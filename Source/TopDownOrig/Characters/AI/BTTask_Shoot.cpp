// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_Shoot.h"
#include "Characters/ProjectileAttackComponent.h"
#include <AIController.h>
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BlackboardComponent.h"


UBTTask_Shoot::UBTTask_Shoot()
 {
    bNotifyTick = true;
    bNotifyTaskFinished = true;
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8*)
{
    auto* aiActor = ownerComp.GetAIOwner()->GetPawn();
    ENSURE(aiActor);
    auto* projectileAttack = aiActor->FindComponentByClass<UProjectileAttackComponent>();
    ENSURE(projectileAttack);

    TargetBlackboardKey.ResolveSelectedKey(*ownerComp.GetBlackboardComponent()->GetBlackboardAsset());

    UObject* bBValue = ownerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(TargetBlackboardKey.GetSelectedKeyID());
    auto* target = static_cast<AActor*>(bBValue);

    if (!ENSURE(projectileAttack->IsInRange(target)))
    {
        return EBTNodeResult::Failed;
    }

    auto onFireEnd = [this, ownerCompPtr = &ownerComp]
    {
        FinishLatentTask(*ownerCompPtr, EBTNodeResult::Succeeded);
    };
    projectileAttack->FireAt(target, onFireEnd);
    return EBTNodeResult::InProgress;
}

