// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include <Utils/macro_shortcuts.h>
#include "BTTask_Shoot.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNORIG_API UBTTask_Shoot : public UBTTaskNode
{
	GENERATED_BODY()
    UBTTask_Shoot();

public:
    /** blackboard key selector */
	UPROPERTY(EditAnywhere, Category=Blackboard)
	struct FBlackboardKeySelector TargetBlackboardKey;

    EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
