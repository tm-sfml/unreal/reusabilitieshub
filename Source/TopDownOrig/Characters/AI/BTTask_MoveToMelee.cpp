#include "BTTask_MoveToMelee.h"
#include "AI/NavigationSystemBase.h"
#include "AIController.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include <Characters/MeleeAttackComponent.h>
#include "Utils/utils.h"
#include <Utils/macro_shortcuts.h>

UBTTask_MoveToMelee::UBTTask_MoveToMelee()
    : Super()
{
    bNotifyTick = true;
}

EBTNodeResult::Type UBTTask_MoveToMelee::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8* NodeMemory)
{
    if (StartMoveToTarget(ownerComp))
    {
        return EBTNodeResult::InProgress;
    }
    else
    {
        return EBTNodeResult::Failed;
    }
}

void UBTTask_MoveToMelee::TickTask(UBehaviorTreeComponent& ownerComp, uint8* nodeMemory, float dt)
{
    // re-implementing Acceptable radius of move request, since I need to check distance on XY plane only
    Super::TickTask(ownerComp, nodeMemory, dt);

    auto* bBValue = ownerComp.GetBlackboardComponent()->GetValueAsObject(TargetActorKey.SelectedKeyName);
    auto* target = static_cast<AActor*>(bBValue);
    if (!ENSURE(target))
    {
        FinishLatentTask(ownerComp, EBTNodeResult::Failed);
        return;
    }

    if (!target->IsValidLowLevel())
    {
        FinishLatentTask(ownerComp, EBTNodeResult::Failed);
        return;
    }

    auto* aiController = ownerComp.GetAIOwner();
    auto* melee = aiController->GetPawn()->FindComponentByClass<UMeleeAttackComponent>();
    if (!ENSURE(melee))
    {
        return;
    }


    if (melee->IsInRange(target))
    {
        aiController->StopMovement();
        FinishLatentTask(ownerComp, EBTNodeResult::Succeeded);
    }
    else if (aiController->GetMoveStatus() == EPathFollowingStatus::Idle)
    {
        // if we somehow reached destination and did not reach melee, try again
        bool res = StartMoveToTarget(ownerComp);
        if (!res)
        {
            FinishLatentTask(ownerComp, EBTNodeResult::Failed);
        }
        FinishLatentTask(ownerComp, EBTNodeResult::Succeeded);
    }
}

bool UBTTask_MoveToMelee::StartMoveToTarget(UBehaviorTreeComponent& ownerComp)
{
    auto* aiController = ownerComp.GetAIOwner();
    auto* bBValue = ownerComp.GetBlackboardComponent()->GetValueAsObject(TargetActorKey.SelectedKeyName);
    auto* target = static_cast<AActor*>(bBValue);
    if (!target)
    {
        ERROR_LOG("target is null");
        return false;
    }

    aiController->MoveToActor(target);
    return true;
}
