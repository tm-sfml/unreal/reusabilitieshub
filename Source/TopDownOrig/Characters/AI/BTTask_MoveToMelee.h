#pragma once
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BehaviorTree/Tasks/BTTask_MoveTo.h"
#include "CoreMinimal.h"
#include "BTTask_MoveToMelee.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNORIG_API UBTTask_MoveToMelee : public UBTTaskNode
{
    GENERATED_BODY()

public:
    UBTTask_MoveToMelee();

    EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
    void TickTask(UBehaviorTreeComponent& OwnerComp,
                  uint8* NodeMemory,
                  float DeltaSeconds) override;

private:
    UPROPERTY(EditAnywhere, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    FBlackboardKeySelector TargetActorKey;

    bool StartMoveToTarget(UBehaviorTreeComponent& ownerComp);
};
