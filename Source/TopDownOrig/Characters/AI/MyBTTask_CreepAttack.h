

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "MyBTTask_CreepAttack.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNORIG_API UMyBTTask_CreepAttack : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
public:
    EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
    UPROPERTY(EditAnywhere, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    FBlackboardKeySelector StopAttackConditionKey;

    UPROPERTY(EditAnywhere, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    bool UseStopAttackCondition = false;

    FDelegateHandle OnTargetDeathDelegateHandle;
};
