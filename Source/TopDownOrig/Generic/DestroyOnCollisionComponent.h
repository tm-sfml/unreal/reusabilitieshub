#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DestroyOnCollisionComponent.generated.h"

class UPrimitiveComponent;
class AActor;

// destroys the parent when it hits actor with CollisionActorTag tag
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOPDOWNORIG_API UDestroyOnOverlapComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UDestroyOnOverlapComponent();

protected:
	virtual void BeginPlay() override;

private:
    UPROPERTY(EditAnywhere, Category = ProjectileAttack, meta=(AllowPrivateAccess = true))
	FName CollisionActorTag;

    UFUNCTION()
    void OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

    UFUNCTION()
    void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
