#include "DestroyOnCollisionComponent.h"
#include "GameFramework/Actor.h"
#include <Components/PrimitiveComponent.h>
#include <Components/BoxComponent.h>
#include <Utils/macro_shortcuts.h>


UDestroyOnOverlapComponent::UDestroyOnOverlapComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UDestroyOnOverlapComponent::BeginPlay()
{
	Super::BeginPlay(); 
    auto* shapeComponent = GetOwner()->FindComponentByClass<UPrimitiveComponent>();

    if (!ENSURE(shapeComponent))
    {
        return;
    }
    ENSURE_MSG(GetOwner()->GetRootComponent() == shapeComponent, "[UDestroyOnOverlapComponent] Shape component of owner is not root");
    ENSURE(shapeComponent->GetGenerateOverlapEvents());
    shapeComponent->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnBeginOverlap);
    //shapeComponent->OnComponentHit.AddDynamic(this, &ThisClass::OnCompHit);
}

void UDestroyOnOverlapComponent::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    DEBUG_LOG_FUNC()

    if (!OtherActor)
    {
        return;
    }

    if (OtherActor != GetOwner() && OtherActor->ActorHasTag(CollisionActorTag))
	{
        GetOwner()->Destroy();
	}
}

void UDestroyOnOverlapComponent::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 , bool, const FHitResult&) 
{
    DEBUG_LOG_FUNC()
    if (!OtherActor)
    {
        return;
    }

    if (OtherActor != GetOwner() && OtherActor->ActorHasTag(CollisionActorTag))
	{
        GetOwner()->Destroy();
	}
}