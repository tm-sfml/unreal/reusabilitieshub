// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "AbilityIconWidget.generated.h"

class UAbility;
class UProgressBar;

UCLASS()
class TOPDOWNORIG_API UAbilityIconWidget : public UUserWidget
{
    GENERATED_BODY()
public:
    UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
    UProgressBar* CooldownProgressBar;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ability)
    TSubclassOf<UAbility> AbilityClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ability)
    FSlateBrush AbilityImage;

    // 0 for ready, 1 for full cooldown
    UFUNCTION(BlueprintCallable, Category = Ability)
    float GetCooldownPercent() const;

protected:
    void NativeConstruct() override;

private:
    UAbility const* Ability = nullptr;

    void OnAbilityUnlocked(UAbility const& ability);
};
