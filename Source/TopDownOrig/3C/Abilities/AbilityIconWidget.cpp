#include "AbilityIconWidget.h"
#include "Components/ProgressBar.h"
#include "Styling/SlateTypes.h"
#include <3C/Abilities/AbilitiesComponent.h>
#include <Utils/macro_shortcuts.h>


float UAbilityIconWidget::GetCooldownPercent() const
{
    if (!Ability)
    {
        return 1.f;
    }
    return Ability->GetCooldownPercent();
}

void UAbilityIconWidget::NativeConstruct()
{
    Super::NativeConstruct();

    auto* abilities = UAbilitiesComponent::GetGlobalUAbilitiesComponent(GetWorld());
    if (!abilities)
    {
        return;
    }

    abilities->AbilityUnlockedEvent.AddUObject(this, &ThisClass::OnAbilityUnlocked);
}

void UAbilityIconWidget::OnAbilityUnlocked(UAbility const& ability)
{
    if (!CooldownProgressBar)
    {
        ERROR_LOG("CooldownProgressBar is null");
        return;
    }

    if (ability.GetClass() != AbilityClass)
    {
        return;
    }

    Ability = &ability;
    CooldownProgressBar->WidgetStyle.SetBackgroundImage(AbilityImage);
}
