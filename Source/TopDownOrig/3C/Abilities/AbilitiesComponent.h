#pragma once

#include "Ability.h"
#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include <Utils/macro_shortcuts.h>
#include <memory>
#include "AbilitiesComponent.generated.h"

class UInputComponent;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOPDOWNORIG_API UAbilitiesComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    DECLARE_EVENT_OneParam(ThisClass, FAbilityUnlockedEvent, UAbility const&);

    // it's only one, added to player
    static UAbilitiesComponent* GetGlobalUAbilitiesComponent(UWorld* world);

    FAbilityUnlockedEvent AbilityUnlockedEvent;
    //UAbility* TryGetAbility(TSubclassOf<UAbility> abilityType);

    bool IsAbilitySelected() const { return TargetingAbilityType.Get() != nullptr; }
    void SelectAbilityForTargeting(TSubclassOf<UAbility> type);
    void DeselectTargetingAbility() { TargetingAbilityType = nullptr; }
    void UseSelectedAbility(FVector mouseHitLocation);

    // TODO: templates not needed
    // TAbility is subclass of UAbility
    template <class TAbility>
    void UnlockAbility()
    {
        auto const type = TAbility::StaticClass();

        if (UnlockedAbilities.Contains(type))
        {
            ERROR_LOG("ability already unlocked");
            return;
        }

        UnlockedAbilities.Add(type);
        auto* ability = *Abilities.Find(type);
        if (!ability)
        {
            ERROR_LOG("ability missing");
            return;
        }

        AbilityUnlockedEvent.Broadcast(*ability);
    }

private:
    UPROPERTY(EditAnywhere, Instanced, Category = Abilities, meta = (AllowPrivateAccess = "true"))
    TMap<TSubclassOf<UAbility>, UAbility*> Abilities;

    TSet<TSubclassOf<UAbility>> UnlockedAbilities;

    TSubclassOf<UAbility> TargetingAbilityType;
};
