// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TopDownOrigCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownOrigCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATopDownOrigCharacter();

	void SetNewMoveDestination(FVector DestLocation);
};

