// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TopDownOrigPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/Material.h"
#include <Utils/AReferenceHolder.h>
#include <3C/TopDown/TopDownOrigCharacter.h>


ATopDownOrigPlayerController::ATopDownOrigPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATopDownOrigPlayerController::BeginPlay()
{
    Super::BeginPlay();

    if (!AAReferenceHolder::GetInstance())
    {
        return;
    }
    CursorToWorld = AAReferenceHolder::GetInstance()->GetReference<AActor>(WorldActorReference::CursorDecal);
}

void ATopDownOrigPlayerController::PlayerTick(float dt)
{
	Super::PlayerTick(dt);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();

        if (!CursorToWorld)
        {
            return;
        }

        FHitResult traceHitResult;
	    GetHitResultUnderCursor(ECC_Visibility, true, traceHitResult);
	    FVector const cursorFv = traceHitResult.ImpactNormal;
	    FRotator const cursorR = cursorFv.Rotation();
	    CursorToWorld->SetActorLocation(traceHitResult.Location);
	    CursorToWorld->SetActorRotation(cursorR);
	}
}

void ATopDownOrigPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATopDownOrigPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ATopDownOrigPlayerController::OnSetDestinationReleased);


}

void ATopDownOrigPlayerController::MoveToMouseCursor()
{
	// Trace to see what is under the mouse cursor
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	if (!Hit.bBlockingHit)
    {
        return;
    }
	// We hit something, move there
    if (GetCastedCharacter())
	{
        GetCastedCharacter()->SetNewMoveDestination(Hit.ImpactPoint);
    }
}


void ATopDownOrigPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ATopDownOrigPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

ATopDownOrigCharacter* ATopDownOrigPlayerController::GetCastedCharacter() const
{
    return static_cast<ATopDownOrigCharacter*>(GetPawn());
}

