// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TopDownOrigPlayerController.generated.h"

class ATopDownOrigCharacter;


UCLASS()
class ATopDownOrigPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATopDownOrigPlayerController();

private:
 //   /** A decal that projects to the cursor location. */
	//UPROPERTY(EditAnywhere, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class AActor* CursorToWorld = nullptr;

    void BeginPlay() override;

	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
    void PlayerTick(float dt) override;
    void SetupInputComponent() override;
	// End PlayerController interface

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

    ATopDownOrigCharacter* GetCastedCharacter() const;
};


