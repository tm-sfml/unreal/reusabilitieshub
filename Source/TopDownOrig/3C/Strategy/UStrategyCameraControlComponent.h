#pragma once

#include "Camera/CameraComponent.h"
#include "CoreMinimal.h"
#include "UStrategyCameraControlComponent.generated.h"

/**
 *
 */
UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), Config = Game)
class TOPDOWNORIG_API UStrategyCameraControlComponent : public UActorComponent
{
    GENERATED_BODY()
  public:
    UStrategyCameraControlComponent();

    void BeginPlay() override;
    void TickComponent(float dt, enum ELevelTick, FActorComponentTickFunction*) override;

    float GetZoomedOutZ() const { return ZoomedOutZ; }

    void OnZoomIn();
    void OnZoomOut();
    void RotateLeft();
    void RotateRight();

  private:
    /** Size of the area at the edge of the screen that will trigger camera scrolling. */
    UPROPERTY(EditAnywhere, Category = CameraControl, meta = (AllowPrivateAccess = "true"))
    int CameraActiveBorder = 15.f;
#pragma region zoom
    UPROPERTY(EditAnywhere, Category = CameraControl, meta = (AllowPrivateAccess = "true"))
    float ZoomedOutZ;

    UPROPERTY(EditAnywhere, Category = CameraControl, meta = (AllowPrivateAccess = "true"))
    float ZoomValue = 0.7f;

    UPROPERTY(EditAnywhere, Category = CameraControl, meta = (AllowPrivateAccess = "true"))
    float ZoomSpeed = 0.05f;
    /** Minimum amount of camera zoom (How close we can get to the map). */
    UPROPERTY(EditAnywhere, Category = CameraControl, meta = (AllowPrivateAccess = "true"))
    float MinZoomLevel = 0.2f;
#pragma endregion

    UPROPERTY(EditAnywhere, Category = CameraControl, meta = (AllowPrivateAccess = "true"))
    float RotationDegreeDelta = 30.f;

    /** Sets the desired zoom level; clamping if necessary */
    void SetZoomLevel(float newLevel);

    void MoveCamera(FVector dir);

    /** Return the player controller of the pawn that owns this component. */
    APlayerController* GetPlayerController();
};
