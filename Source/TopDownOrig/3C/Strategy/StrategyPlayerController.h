#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "StrategyPlayerController.generated.h"

class UStrategyCameraControlComponent;

/**
 * 
 */
UCLASS()
class TOPDOWNORIG_API AStrategyPlayerController : public APlayerController
{
    GENERATED_BODY()
public:
    AStrategyPlayerController();

protected:
    void SetupInputComponent() override;
};
