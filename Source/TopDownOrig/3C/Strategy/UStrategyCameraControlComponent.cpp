#include "UStrategyCameraControlComponent.h"
#include "GameFramework/SpectatorPawnMovement.h"
#include <Engine/LocalPlayer.h>
#include <Engine/World.h>
#include <GameFramework/Actor.h>
#include <GameFramework/FloatingPawnMovement.h>
#include <GameFramework/PlayerController.h>
#include <GameFramework/SpectatorPawn.h>
#include <GameFramework/SpringArmComponent.h>
#include <Math/UnrealMathUtility.h>
#include <Utils/utils.h>

namespace
{
float const MaxZoomLevel = 1.0f;
}

UStrategyCameraControlComponent::UStrategyCameraControlComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
    PrimaryComponentTick.SetTickFunctionEnable(true);
}

void UStrategyCameraControlComponent::BeginPlay()
{
    Super::BeginPlay();
    SetZoomLevel(ZoomValue);
}

void UStrategyCameraControlComponent::TickComponent(float dt, ELevelTick _1, FActorComponentTickFunction* _2)
{
    Super::TickComponent(dt, _1, _2);

    ULocalPlayer* const LocalPlayer = Cast<ULocalPlayer>(GetPlayerController()->Player);
    if (!(LocalPlayer && LocalPlayer->ViewportClient && LocalPlayer->ViewportClient->Viewport))
    {
        return;
    }

    FVector2D MousePosition;
    if (LocalPlayer->ViewportClient->GetMousePosition(MousePosition) == false)
    {
        return;
    }

    FViewport* Viewport = LocalPlayer->ViewportClient->Viewport;

    const uint32 MouseX = MousePosition.X;
    const uint32 MouseY = MousePosition.Y;

    const FIntPoint ViewportSize = Viewport->GetSizeXY();
    const uint32 ViewLeft = FMath::TruncToInt(LocalPlayer->Origin.X * ViewportSize.X);
    const uint32 ViewRight = ViewLeft + FMath::TruncToInt(LocalPlayer->Size.X * ViewportSize.X);
    const uint32 ViewTop = FMath::TruncToInt(LocalPlayer->Origin.Y * ViewportSize.Y);
    const uint32 ViewBottom = ViewTop + FMath::TruncToInt(LocalPlayer->Size.Y * ViewportSize.Y);

    if (MouseX >= ViewLeft && MouseX <= (ViewLeft + CameraActiveBorder))
    {
        const float delta = 1.0f - float(MouseX - ViewLeft) / CameraActiveBorder;
        MoveCamera(-delta * GetOwner()->GetActorRightVector());
    }
    else if (MouseX >= (ViewRight - CameraActiveBorder) && MouseX <= ViewRight)
    {
        const float delta = float(MouseX - ViewRight + CameraActiveBorder) / CameraActiveBorder;
        MoveCamera(delta * GetOwner()->GetActorRightVector());
    }

    if (MouseY >= ViewTop && MouseY <= (ViewTop + CameraActiveBorder))
    {
        const float delta = 1.0f - float(MouseY - ViewTop) / CameraActiveBorder;
        MoveCamera(delta * GetOwner()->GetActorForwardVector());
    }
    else if (MouseY >= (ViewBottom - CameraActiveBorder) && MouseY <= ViewBottom)
    {
        const float delta = float(MouseY - (ViewBottom - CameraActiveBorder)) / CameraActiveBorder;
        MoveCamera(-delta * GetOwner()->GetActorForwardVector());
    }
}

void UStrategyCameraControlComponent::OnZoomIn()
{
    SetZoomLevel(ZoomValue - ZoomSpeed);
}

void UStrategyCameraControlComponent::OnZoomOut()
{
    SetZoomLevel(ZoomValue + ZoomSpeed);
}

void UStrategyCameraControlComponent::RotateLeft()
{
    auto newRot = GetOwner()->GetActorRotation().Add(0.f, -RotationDegreeDelta, 0.f);
    GetOwner()->SetActorRotation(newRot);
}

void UStrategyCameraControlComponent::RotateRight()
{
    auto newRot = GetOwner()->GetActorRotation().Add(0.f, RotationDegreeDelta, 0.f);
    GetOwner()->SetActorRotation(newRot);
}

void UStrategyCameraControlComponent::MoveCamera(FVector dir)
{
    auto movement = GetOwner()->FindComponentByClass<UFloatingPawnMovement>();
    movement->AddInputVector(dir);
}

APlayerController* UStrategyCameraControlComponent::GetPlayerController()
{
    APlayerController* Controller = NULL;
    APawn* Owner = Cast<APawn>(GetOwner());
    if (Owner != NULL)
    {
        Controller = Cast<APlayerController>(Owner->GetController());
    }
    return Controller;
}

void UStrategyCameraControlComponent::SetZoomLevel(float NewLevel)
{
    auto* camera = GetOwner()->FindComponentByClass<UCameraComponent>();
    if (!ENSURE(camera))
    {
        return;
    }

    ZoomValue = FMath::Clamp(NewLevel, MinZoomLevel, MaxZoomLevel);

    FVector const cameraZoomLinePoint1 = GetOwner()->GetActorLocation();
    FVector const cameraZoomLinePoint2 = cameraZoomLinePoint1 + camera->GetForwardVector();
    FPlane const topPlain(FVector::UpVector * ZoomedOutZ, FVector::UpVector);

    FVector const topPlainPoint = FMath::LinePlaneIntersection(cameraZoomLinePoint1, cameraZoomLinePoint2, topPlain);

    FPlane const floor(FVector::ZeroVector, FVector::UpVector);
    FVector const floorPoint = FMath::LinePlaneIntersection(cameraZoomLinePoint1, cameraZoomLinePoint2, floor);

    float const distanceToFloor = (floorPoint - topPlainPoint).Size();
    float const desiredDistance = distanceToFloor * ZoomValue;

    // using direction of camera, but moving the root actor
    FVector const desiredPosition = topPlainPoint + camera->GetForwardVector() * desiredDistance;
    GetOwner()->SetActorLocation(desiredPosition);
}
