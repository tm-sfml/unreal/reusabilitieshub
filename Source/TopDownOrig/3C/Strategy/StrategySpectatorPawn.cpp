// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "StrategySpectatorPawn.h"
#include "UStrategyCameraControlComponent.h"
#include <Components/InputComponent.h>
#include <Components/SphereComponent.h>
#include <Engine/CollisionProfile.h>
#include <GameFramework/SpectatorPawnMovement.h>
#include <Utils/utils.h>
#include <functional>

AStrategySpectatorPawn::AStrategySpectatorPawn()
    : Super()
{
    bAddDefaultMovementBindings = false;

    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;
}

void AStrategySpectatorPawn::BeginPlay()
{
    Super::BeginPlay();
    auto const* cameraStart = WorldUtils::FindActorInWorld<AActor>(GetWorld(), "CameraStart");
    if (!cameraStart)
    {
        ERROR_LOG("camera start not found");
        return;
    }

    SetActorLocation(cameraStart->GetActorLocation());


    if (StrategyCameraComponent && GetActorLocation().Z > StrategyCameraComponent->GetZoomedOutZ())
    {
        ERROR_LOG("camera start Z > camera control ZoomedOutZ");
    }
}

void AStrategySpectatorPawn::SetupPlayerInputComponent(UInputComponent* input)
{
    check(input);

    auto* cameraControl = FindComponentByClass<UStrategyCameraControlComponent>();

    input->BindAction("ZoomIn", IE_Pressed, cameraControl, &UStrategyCameraControlComponent::OnZoomIn);
    input->BindAction("ZoomOut", IE_Pressed, cameraControl, &UStrategyCameraControlComponent::OnZoomOut);
    input->BindAction("RotateLeft", IE_Pressed, cameraControl, &UStrategyCameraControlComponent::RotateLeft);
    input->BindAction("RotateRight", IE_Pressed, cameraControl, &UStrategyCameraControlComponent::RotateRight);

    input->BindAxis("MoveForward", this, &ThisClass::MoveForward);
    input->BindAxis("MoveRight", this, &ThisClass::MoveRight);
}

void AStrategySpectatorPawn::MoveForward(float val)
{
    Move(GetActorForwardVector() * val);
}

void AStrategySpectatorPawn::MoveRight(float val)
{
    Move(GetActorRightVector() * val);
}

void AStrategySpectatorPawn::Move(FVector dir)
{
    auto* movement = FindComponentByClass<UFloatingPawnMovement>();
    movement->AddInputVector(dir);
}
