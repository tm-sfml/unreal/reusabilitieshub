#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "StrategySpectatorPawn.generated.h"


/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TOPDOWNORIG_API AStrategySpectatorPawn : public ASpectatorPawn
{
	GENERATED_BODY()

    AStrategySpectatorPawn();
    
    void BeginPlay() override;
	void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

private:
	UPROPERTY(Category = CameraActor, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStrategyCameraControlComponent* StrategyCameraComponent;

    void MoveForward(float val);
    void MoveRight(float val);
    void Move(FVector dir);
};
