#include "StrategyPlayerController.h"
#include "StrategySpectatorPawn.h"
#include "UStrategyCameraControlComponent.h"
#include <DrawDebugHelpers.h>
#include <World/Tiles/TileHighlightComponent.h>
#include <World/Tiles/WorldTilemapComponent.h>
#include <Utils/AReferenceHolder.h>
#include <Utils/macro_shortcuts.h>
#include <Utils/utils.h>

AStrategyPlayerController::AStrategyPlayerController()
    : Super()
{
    PrimaryActorTick.bCanEverTick = true;
    SetHidden(false);
    bShowMouseCursor = true;
}

void AStrategyPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    auto l = [this] {
        DEBUG_LOG(TEXT("click"));

        FHitResult hit;
        GetHitResultUnderCursor(ECC_Visibility, true, hit);
        DrawDebugPoint(GetWorld(), hit.Location, 10.f, FColor::Green, false, 5.f);

        auto* tiles = hit.Actor->FindComponentByClass<UWorldTilemapComponent>();
        if (!tiles)
        {
            return;
        }

        auto const index = tiles->GetTileIndex(hit.ImpactPoint);
        DEBUG_LOGF("Index: %s", *index.ToString());
        auto* highlighter = hit.Actor->FindComponentByClass<UTileHighlightComponent>();
        if (!highlighter)
        {
            return;
        }

        if (highlighter->IsTileHighlighted(index))
        {
            highlighter->UnhighlightAllTiles();
        }
        else
        {
            highlighter->HighlightTile(index);
        }

        //DEBUG_LOGF("hit.Location: %s", *hit.Location.ToString());
    };
    C3Utils::BindLambdaToAction("LeftClick", IE_Pressed, l, InputComponent);
}
