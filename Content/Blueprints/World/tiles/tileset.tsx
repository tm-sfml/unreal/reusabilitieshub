<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="tileset" tilewidth="35" tileheight="32" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="35" height="32" source="floor.png"/>
 </tile>
 <tile id="1">
  <image width="35" height="32" source="wall.png"/>
 </tile>
</tileset>
